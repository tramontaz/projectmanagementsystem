package net.chemodurov.projectmanagement.dao.jdbc;

import net.chemodurov.projectmanagement.dao.TeamDAO;
import net.chemodurov.projectmanagement.dao.db.DBHelper;
import net.chemodurov.projectmanagement.dao.db.SQLRuntimeException;
import net.chemodurov.projectmanagement.model.Developer;
import net.chemodurov.projectmanagement.model.Skill;
import net.chemodurov.projectmanagement.model.Team;

import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class JdbcTeamDAOImpl implements TeamDAO {
    private DBHelper dbHelper;

    public JdbcTeamDAOImpl(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public void insert(Team team) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement(
                    "INSERT INTO teams (id, name) " +
                            "SELECT * FROM (SELECT ?, ?) AS checkRep " +
                            "WHERE NOT EXISTS (SELECT * FROM teams WHERE id = ? AND name = ?) LIMIT 1;");
            dbHelper.prepStatementSetString(1, String.valueOf(team.getUuid()));
            dbHelper.prepStatementSetString(2, team.getName());
            dbHelper.prepStatementSetString(3, String.valueOf(team.getUuid()));
            dbHelper.prepStatementSetString(4, team.getName());
            dbHelper.prepStatementExecuteUpdate();

            //add developers into the team:
            dbHelper.createPrepareStatement("INSERT INTO team_developers (team_id, dev_id) " +
                    "SELECT * FROM (SELECT ?, ?) AS checkRep " +
                    "WHERE NOT EXISTS (SELECT * FROM team_developers WHERE team_id = ? AND dev_id = ?) LIMIT 1;");
            for (Developer developer : team.getSet()) {
                dbHelper.prepStatementSetString(1, String.valueOf(team.getUuid()));
                dbHelper.prepStatementSetString(2, String.valueOf(developer.getUuid()));
                dbHelper.prepStatementSetString(3, String.valueOf(team.getUuid()));
                dbHelper.prepStatementSetString(4, String.valueOf(developer.getUuid()));
                dbHelper.prepareStatementAddBatch();
            }
            dbHelper.prepareStatementExecuteBatch();

        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public void update(Team team) {
        Set<Developer> newDevelopers = team.getSet();
        Set<Developer> oldDevelopers = new HashSet<>();

        try {
            dbHelper.connect();
            //get old teams's developers from db:
            dbHelper.createPrepareStatement("SELECT dev_id, developers.firstName, developers.lastName, developers.specialty, developers.salary " +
                    "FROM team_developers, developers WHERE team_id = ? AND dev_id = developers.id;");
            dbHelper.prepStatementSetString(1, String.valueOf(team.getUuid()));
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet != null) {
                while (resultSet.next())
                    oldDevelopers.add(new Developer(UUID.fromString(resultSet.getString("dev_id")), resultSet.getString("firstName"),
                            resultSet.getString("lastName"), resultSet.getString("specialty"),
                            new HashSet<Skill>(),
                            resultSet.getBigDecimal("salary")));
            } else throw new Exception("Developers table is empty.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally

        {
            dbHelper.disconnect();
        }

        for (Developer dev : oldDevelopers) {
            dev.setSet(selectSkillsForDev(String.valueOf(dev.getUuid())));
        }
        try {

            //update developer:
            dbHelper.connect();
            dbHelper.createPrepareStatement(
                    "UPDATE teams SET name= ? WHERE id= ?;");
            dbHelper.prepStatementSetString(1, team.getName());
            dbHelper.prepStatementSetString(2, String.valueOf(team.getUuid()));
            dbHelper.prepStatementExecuteUpdate();

            //update team's developers:
            if (!newDevelopers.equals(oldDevelopers)) {
                //delete old skills
                for (Developer oldDev : oldDevelopers) {
                    if (!newDevelopers.contains(oldDev)) {
                        dbHelper.createPrepareStatement("DELETE FROM team_developers WHERE team_id = ? AND dev_id = ?;");
                        dbHelper.prepStatementSetString(1, String.valueOf(team.getUuid()));
                        dbHelper.prepStatementSetString(2, String.valueOf(oldDev.getUuid()));
                        dbHelper.prepStatementExecuteUpdate();
                    }
                }
                //add new developers into the team
                for (Developer newDev : newDevelopers) {
                    if (!oldDevelopers.contains(newDev)) {
                        dbHelper.createPrepareStatement("INSERT INTO team_developers (team_id, dev_id) VALUES (?, ?);");
                        dbHelper.prepStatementSetString(1, String.valueOf(team.getUuid()));
                        dbHelper.prepStatementSetString(2, String.valueOf(newDev.getUuid()));
                        dbHelper.prepStatementExecuteUpdate();
                    }
                }
            }
        } catch (
                Exception e)

        {
            e.printStackTrace();
        } finally

        {
            dbHelper.disconnect();
        }
    }

    @Override
    public void delete(UUID id) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("DELETE FROM teams WHERE id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(id));
            dbHelper.prepStatementExecuteUpdate();

            dbHelper.createPrepareStatement("DELETE FROM team_developers WHERE team_id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(id));
            dbHelper.prepStatementExecuteUpdate();
        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public Team getById(UUID id) {
        Team team;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM teams WHERE id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(id));
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet.next()) {
                    team = new Team(UUID.fromString(resultSet.getString("id")), resultSet.getString("name"),
                            new HashSet<Developer>());
                } else throw new Exception("There is no teams with that name...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }
        team.setSet(selectDevForTeam(String.valueOf(team.getUuid())));
        return team;
    }

    @Override
    public Team getByName(String name) {
        Team team;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM teams WHERE name = ?;");
            dbHelper.prepStatementSetString(1, name);
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet.next()) {
                    team = new Team(UUID.fromString(resultSet.getString("id")), resultSet.getString("name"),
                            new HashSet<Developer>());
                } else throw new Exception("There is no teams with that name...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }
        //add developers ito the team:
        team.setSet(selectDevForTeam(String.valueOf(team.getUuid())));
        return team;
    }


    @Override
    public Set<Team> getAll() {
        Set<Team> teams = new HashSet<>();
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM teams;");
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet != null) {
                    while (resultSet.next()) {
                        teams.add(new Team(UUID.fromString(resultSet.getString("id")), resultSet.getString("name"),
                                new HashSet<Developer>()));
                    }
                } else throw new Exception("There is no teams...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }
        for (Team team : teams) {
            team.setSet(selectDevForTeam(String.valueOf(team.getUuid())));
        }
        return teams;
    }

    @Override
    public Developer getDevByName(String firstName, String lastName) {
        Developer developer;
        Set<Skill> devSkills;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM developers WHERE firstName = ? AND lastName = ?;");
            dbHelper.prepStatementSetString(1, firstName);
            dbHelper.prepStatementSetString(2, lastName);
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet.next()) {
                    developer = new Developer(UUID.fromString(resultSet.getString("id")), resultSet.getString("firstName"),
                            resultSet.getString("lastName"), resultSet.getString("specialty"),
                            new HashSet<Skill>(),
                            resultSet.getBigDecimal("salary"));
                } else throw new Exception("There is no developers with that name...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }
        devSkills = selectSkillsForDev(String.valueOf(developer.getUuid()));
        developer.setSet(devSkills);
        return developer;
    }

    @Override
    public void deleteDevFromTeam(UUID id) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("DELETE FROM team_developers WHERE team_id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(id));
            dbHelper.prepStatementExecuteUpdate();
        } finally {
            dbHelper.disconnect();
        }
    }

    private Set<Skill> selectSkillsForDev(String uuid) {
        Set<Skill> skills = null;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT skill_id, skills.name FROM developer_skills, skills " +
                    "WHERE dev_id = ? AND skill_id = skills.id;");
            dbHelper.prepStatementSetString(1, uuid);
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet != null) {
                skills = new HashSet<>();
                while (resultSet.next()) {
                    skills.add(new Skill(UUID.fromString(resultSet.getString("skill_id")), resultSet.getString("name")));
                }
            } else throw new Exception("There is no skills...");
            //close resultSet
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbHelper.disconnect();
        }
        return skills;
    }

    private Set<Developer> selectDevForTeam(String uuid) {
        Set<Developer> developers = new HashSet<>();
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement(
                    "SELECT dev_id, developers.firstName, developers.lastName, developers.specialty, developers.salary " +
                            "FROM team_developers, developers " +
                            "WHERE team_id = ? AND dev_id = developers.id;");
            dbHelper.prepStatementSetString(1, uuid);
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet != null) {
                while (resultSet.next())
                    developers.add(new Developer(UUID.fromString(resultSet.getString("dev_id")), resultSet.getString("firstName"),
                            resultSet.getString("lastName"), resultSet.getString("specialty"),
                            new HashSet<Skill>(),
                            resultSet.getBigDecimal("salary")));
            } else throw new Exception("Developers table is empty.");
        } catch (Exception e) {
            throw new SQLRuntimeException(e);
        } finally {
            dbHelper.disconnect();
        }
        for (Developer dev : developers) {
            dev.setSet(selectSkillsForDev(String.valueOf(dev.getUuid())));
        }
        return developers;
    }
}
