package net.chemodurov.projectmanagement.dao.jdbc;


import net.chemodurov.projectmanagement.dao.SkillDAO;
import net.chemodurov.projectmanagement.dao.db.DBHelper;
import net.chemodurov.projectmanagement.dao.db.SQLRuntimeException;
import net.chemodurov.projectmanagement.model.Skill;

import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class JdbcSkillDAOImpl implements SkillDAO {
    private DBHelper dbHelper;

    public JdbcSkillDAOImpl(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
    }


    @Override
    public void insert(Skill skill) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement(
                    "INSERT INTO skills (id, name) " +
                            "SELECT * FROM (SELECT ?, ?) AS checkRep " +
                            "WHERE NOT EXISTS (SELECT * FROM skills WHERE id = ? AND name = ?) LIMIT 1;");
            dbHelper.prepStatementSetString(1, String.valueOf(skill.getUuid()));
            dbHelper.prepStatementSetString(2, skill.getName());
            dbHelper.prepStatementSetString(3, String.valueOf(skill.getUuid()));
            dbHelper.prepStatementSetString(4, skill.getName());
            dbHelper.prepStatementExecuteUpdate();
        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public void update(Skill skill) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("UPDATE skills SET name = ? WHERE id = ?;");
            dbHelper.prepStatementSetString(1, skill.getName());
            dbHelper.prepStatementSetString(2, String.valueOf(skill.getUuid()));
            dbHelper.prepStatementExecuteUpdate();
        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public void delete(UUID id) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("DELETE FROM skills WHERE id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(id));
            dbHelper.prepStatementExecuteUpdate();
        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public Skill getById(UUID id) {
        Skill skill;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT id, name FROM skills WHERE id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(id));
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet.next()) {
                skill = new Skill(UUID.fromString(resultSet.getString(1)), resultSet.getString(2));
            } else throw new Exception("There is no skill with that id!");

        } catch (Exception e) {
            throw new SQLRuntimeException(e);
        } finally {
            dbHelper.disconnect();
        }
        return skill;
    }

    @Override
    public Skill getByName(String name) {
        Skill skill;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT id, name FROM skills WHERE name = ?;");
            dbHelper.prepStatementSetString(1, name);
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet.next()) {
                skill = new Skill(UUID.fromString(resultSet.getString("id")), resultSet.getString("name"));
            } else throw new Exception("There is no skill with that name!");

        } catch (Exception e) {
            throw new SQLRuntimeException(e);
        } finally {
            dbHelper.disconnect();
        }
        return skill;
    }

    @Override
    public Set<Skill> getAll() {
        Set<Skill> skills = new HashSet<>();
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM skills;");
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet != null) {
                while (resultSet.next())
                    skills.add(new Skill(UUID.fromString(resultSet.getString("id")), resultSet.getString("name")));
            } else throw new Exception("Skills table is empty.");

        } catch (Exception e) {
            throw new SQLRuntimeException(e);
        } finally {
            dbHelper.disconnect();
        }
        return skills;
    }
}
