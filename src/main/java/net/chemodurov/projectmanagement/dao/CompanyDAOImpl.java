package net.chemodurov.projectmanagement.dao;

import net.chemodurov.projectmanagement.dao.db.DBHelper;
import net.chemodurov.projectmanagement.dao.db.SQLRuntimeException;
import net.chemodurov.projectmanagement.model.*;

import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class CompanyDAOImpl implements CompanyDAO {
    private DBHelper dbHelper;

    public CompanyDAOImpl(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public void insert(Company company) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement(
                    "INSERT INTO companies (id, name) " +
                            "SELECT * FROM (SELECT ?, ?) AS checkRep " +
                            "WHERE NOT EXISTS (SELECT * FROM companies WHERE id = ? AND name = ?) LIMIT 1;");
            dbHelper.prepStatementSetString(1, String.valueOf(company.getUuid()));
            dbHelper.prepStatementSetString(2, company.getName());
            dbHelper.prepStatementSetString(3, String.valueOf(company.getUuid()));
            dbHelper.prepStatementSetString(4, company.getName());
            dbHelper.prepStatementExecuteUpdate();

            //add team into the project:
            dbHelper.createPrepareStatement("INSERT INTO company_projects (company_id , project_id) " +
                    "SELECT * FROM (SELECT ?, ?) AS checkRep " +
                    "WHERE NOT EXISTS (SELECT * FROM company_projects WHERE company_id = ? AND project_id = ?) LIMIT 1;");
            for (Project project : company.getSet()) {
                dbHelper.prepStatementSetString(1, String.valueOf(company.getUuid()));
                dbHelper.prepStatementSetString(2, String.valueOf(project.getUuid()));
                dbHelper.prepStatementSetString(3, String.valueOf(company.getUuid()));
                dbHelper.prepStatementSetString(4, String.valueOf(project.getUuid()));
                dbHelper.prepareStatementAddBatch();
            }
            dbHelper.prepareStatementExecuteBatch();

        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public void update(Company company) {
        Set<Project> newProjects = company.getSet();
        Set<Project> oldProjects = new HashSet<>();

        try {
            dbHelper.connect();
            //get old teams's developers from db:
            dbHelper.createPrepareStatement("SELECT project_id, projects.name " +
                    "FROM company_projects, projects WHERE company_id = ? AND project_id = projects.id;");
            dbHelper.prepStatementSetString(1, String.valueOf(company.getUuid()));
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet != null) {
                while (resultSet.next())
                    oldProjects.add(new Project(UUID.fromString(
                            resultSet.getString("project_id")),
                            resultSet.getString("name"),
                            new HashSet<Team>()));
            } else throw new Exception("Projects table is empty.");
        } catch (Exception e) {
            e.printStackTrace();
        } finally

        {
            dbHelper.disconnect();
        }

        for (Project project : oldProjects) {
            project.setSet(selectTeamsForProject(String.valueOf(project.getUuid())));
        }
        try {

            //update company:
            dbHelper.connect();
            dbHelper.createPrepareStatement(
                    "UPDATE companies SET name= ? WHERE id= ?;");
            dbHelper.prepStatementSetString(1, company.getName());
            dbHelper.prepStatementSetString(2, String.valueOf(company.getUuid()));
            dbHelper.prepStatementExecuteUpdate();

            //update company's projects:
            if (!newProjects.equals(oldProjects)) {
                //delete old skills
                for (Project oldProject : oldProjects) {
                    if (!newProjects.contains(oldProject)) {
                        dbHelper.createPrepareStatement(
                                "DELETE FROM company_projects " +
                                        "WHERE company_id = ? AND project_id = ?;");
                        dbHelper.prepStatementSetString(1, String.valueOf(company.getUuid()));
                        dbHelper.prepStatementSetString(2, String.valueOf(oldProject.getUuid()));
                        dbHelper.prepStatementExecuteUpdate();
                    }
                }
                //add new projects into the company
                for (Project newProject : newProjects) {
                    if (!oldProjects.contains(newProject)) {
                        dbHelper.createPrepareStatement(
                                "INSERT INTO company_projects (company_id, project_id) " +
                                        "VALUES (?, ?);");
                        dbHelper.prepStatementSetString(1, String.valueOf(company.getUuid()));
                        dbHelper.prepStatementSetString(2, String.valueOf(newProject.getUuid()));
                        dbHelper.prepStatementExecuteUpdate();
                    }
                }
            }
        } catch (
                Exception e)

        {
            e.printStackTrace();
        } finally

        {
            dbHelper.disconnect();
        }
    }

    @Override
    public void delete(UUID uuid) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("DELETE FROM companies WHERE id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(uuid));
            dbHelper.prepStatementExecuteUpdate();

            dbHelper.createPrepareStatement("DELETE FROM company_projects WHERE company_id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(uuid));
            dbHelper.prepStatementExecuteUpdate();
        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public Company getById(UUID uuid) {
        Company company;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM companies WHERE id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(uuid));
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet.next()) {
                    company = new Company(UUID.fromString(resultSet.getString("id")), resultSet.getString("name"),
                            new HashSet<Project>());
                } else throw new Exception("There is no company with that name...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }

        company.setSet(selectProjectsForCompany(String.valueOf(company.getUuid())));
        return company;
    }

    @Override
    public Company getByName(String name) {
        Company company;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM companies WHERE name = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(name));
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet.next()) {
                    company = new Company(UUID.fromString(resultSet.getString("id")), resultSet.getString("name"),
                            new HashSet<Project>());
                } else throw new Exception("There is no company with that name...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }

        company.setSet(selectProjectsForCompany(String.valueOf(company.getUuid())));
        return company;
    }

    @Override
    public Set<Company> getAll() {
        Set<Company> companies = new HashSet<>();
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM companies;");
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet != null) {
                    while (resultSet.next()) {
                        companies.add(new Company(UUID.fromString(resultSet.getString("id")), resultSet.getString("name"),
                                new HashSet<Project>()));
                    }
                } else throw new Exception("There is no companies...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }
        for (Company company : companies) {
            company.setSet(selectProjectsForCompany(String.valueOf(company.getUuid())));
        }
        return companies;
    }

    @Override
    public Project getProjectByName(String name) {
        Project project;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM projects WHERE name = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(name));
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet.next()) {
                    project = new Project(UUID.fromString(resultSet.getString("id")), resultSet.getString("name"),
                            new HashSet<Team>());
                } else throw new Exception("There is no projects with that name...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }

        project.setSet(selectTeamsForProject(String.valueOf(project.getUuid())));
        return project;
    }

    @Override
    public void deleteProjectsFromCompany(UUID uuid) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("DELETE FROM company_projects WHERE company_id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(uuid));
            dbHelper.prepStatementExecuteUpdate();
        } finally {
            dbHelper.disconnect();
        }
    }

    private Set<Project> selectProjectsForCompany(String uuid) {
        Set<Project> projects = new HashSet<>();
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT project_id, projects.name " +
                    "FROM company_projects, projects " +
                    "WHERE company_id = ? AND project_id = projects.id;");
            dbHelper.prepStatementSetString(1, uuid);
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet != null) {
                    while (resultSet.next()) {
                        projects.add(new Project(UUID.fromString(resultSet.getString("project_id")), resultSet.getString("name"),
                                new HashSet<Team>()));
                    }
                } else throw new Exception("There is no projects with that name...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }
        for (Project project : projects) {
            project.setSet(selectTeamsForProject(String.valueOf(project.getUuid())));
        }
        return projects;
    }

    private Set<Team> selectTeamsForProject(String uuid) {
        Set<Team> teams = new HashSet<>();
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT team_id, teams.name " +
                    "FROM project_teams, teams " +
                    "WHERE project_id = ? AND team_id = teams.id;");
            dbHelper.prepStatementSetString(1, uuid);
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet != null) {
                    while (resultSet.next()) {
                        teams.add(new Team(UUID.fromString(resultSet.getString("team_id")), resultSet.getString("name"),
                                new HashSet<Developer>()));
                    }
                } else throw new Exception("There is no teams with that name...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }
        for (Team team : teams) {
            team.setSet(selectDevsForTeam(String.valueOf(team.getUuid())));
        }
        return teams;
    }

    private Set<Developer> selectDevsForTeam(String uuid) {
        Set<Developer> developers = new HashSet<>();
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement(
                    "SELECT dev_id, developers.firstName, developers.lastName, developers.specialty, developers.salary " +
                            "FROM developer_teams, developers " +
                            "WHERE team_id = ? AND dev_id = developers.id;");
            dbHelper.prepStatementSetString(1, uuid);
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet != null) {
                while (resultSet.next())
                    developers.add(new Developer(UUID.fromString(resultSet.getString("dev_id")), resultSet.getString("firstName"),
                            resultSet.getString("lastName"), resultSet.getString("specialty"),
                            new HashSet<Skill>(),
                            resultSet.getBigDecimal("salary")));
            } else System.err.println("Developers table is empty.");
        } catch (Exception e) {
            throw new SQLRuntimeException(e);
        } finally {
            dbHelper.disconnect();
        }
        for (Developer dev : developers) {
            dev.setSet(selectSkillsForDev(String.valueOf(dev.getUuid())));
        }
        return developers;
    }

    private Set<Skill> selectSkillsForDev(String uuid) {
        Set<Skill> skills = null;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT skill_id, skills.name FROM developer_skills, skills " +
                    "WHERE dev_id = ? AND skill_id = skills.id;");
            dbHelper.prepStatementSetString(1, uuid);
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet != null) {
                skills = new HashSet<>();
                while (resultSet.next()) {
                    skills.add(new Skill(UUID.fromString(resultSet.getString("skill_id")), resultSet.getString("name")));
                }
            } else throw new Exception("Developer hasn't skills...");
            //close resultSet
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbHelper.disconnect();
        }
        return skills;
    }
}
