package net.chemodurov.projectmanagement.dao;

import net.chemodurov.projectmanagement.dao.db.DBHelper;
import net.chemodurov.projectmanagement.dao.db.SQLRuntimeException;
import net.chemodurov.projectmanagement.model.Developer;
import net.chemodurov.projectmanagement.model.Skill;

import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class DeveloperDAOImpl implements DeveloperDAO {
    private DBHelper dbHelper;

    public DeveloperDAOImpl(DBHelper dbHelper) {
        this.dbHelper = dbHelper;
    }

    @Override
    public void insert(Developer developer) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement(
                    "INSERT INTO developers (id, firstName, lastName, specialty, salary) " +
                            "SELECT * FROM (SELECT ?, ?, ?, ?, ?) AS checkRep " +
                            "WHERE NOT EXISTS (SELECT * FROM developers WHERE id = ? AND firstName = ? AND lastName = ? AND" +
                            " specialty = ? AND salary = ?) LIMIT 1;");
            dbHelper.prepStatementSetString(1, String.valueOf(developer.getUuid()));
            dbHelper.prepStatementSetString(2, developer.getFirstName());
            dbHelper.prepStatementSetString(3, developer.getLastName());
            dbHelper.prepStatementSetString(4, developer.getSpecialty());
            dbHelper.prepStatementSetBigDecimal(5, developer.getSalary());
            dbHelper.prepStatementSetString(6, String.valueOf(developer.getUuid()));
            dbHelper.prepStatementSetString(7, developer.getFirstName());
            dbHelper.prepStatementSetString(8, developer.getLastName());
            dbHelper.prepStatementSetString(9, developer.getSpecialty());
            dbHelper.prepStatementSetBigDecimal(10, developer.getSalary());
            dbHelper.prepStatementExecuteUpdate();

            //add developers skills:
            dbHelper.createPrepareStatement("INSERT INTO developer_skills (dev_id, skill_id) " +
                    "SELECT * FROM (SELECT ?, ?) AS checkRep " +
                    "WHERE NOT EXISTS (SELECT * FROM developer_skills WHERE dev_id = ? AND skill_id = ?) LIMIT 1;");
            for (Skill skills : developer.getSet()) {
                dbHelper.prepStatementSetString(1, String.valueOf(developer.getUuid()));
                dbHelper.prepStatementSetString(2, String.valueOf(skills.getUuid()));
                dbHelper.prepStatementSetString(3, String.valueOf(developer.getUuid()));
                dbHelper.prepStatementSetString(4, String.valueOf(skills.getUuid()));
                dbHelper.prepareStatementAddBatch();
            }
            dbHelper.prepareStatementExecuteBatch();

        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public void update(Developer developer) {
        try {
            dbHelper.connect();

            Set<Skill> newSkills = developer.getSet();

            //get old developer's skills from db:
            Set<Skill> oldSkills = new HashSet<>();
            dbHelper.createPrepareStatement(
                    "SELECT skill_id, skills.name FROM developer_skills, skills " +
                    "WHERE dev_id = ? AND skill_id = skills.id;");
            dbHelper.prepStatementSetString(1, String.valueOf(developer.getUuid()));
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet.next()) {
                    oldSkills.add(new Skill(UUID.fromString(resultSet.getString("skill_id")), resultSet.getString("name")));
                } else System.out.println("There is no skills...");

            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }

            //update developer:
            dbHelper.createPrepareStatement(
                    "UPDATE developers SET firstName= ?, lastName= ?, specialty = ?, salary= ? " +
                            "WHERE id= ?;\n");
            dbHelper.prepStatementSetString(1, developer.getFirstName());
            dbHelper.prepStatementSetString(2, developer.getLastName());
            dbHelper.prepStatementSetString(3, developer.getSpecialty());
            dbHelper.prepStatementSetBigDecimal(4, developer.getSalary());
            dbHelper.prepStatementSetString(5, String.valueOf(developer.getUuid()));
            dbHelper.prepStatementExecuteUpdate();

            //update developer's skills:
            if (!newSkills.equals(oldSkills)) {
                //delete old skills
                for (Skill oldSkill : oldSkills) {
                    if (!newSkills.contains(oldSkill)) {
                        dbHelper.createPrepareStatement("DELETE FROM developer_skills WHERE dev_id = ? AND skill_id = ?;");
                        dbHelper.prepStatementSetString(1, String.valueOf(developer.getUuid()));
                        dbHelper.prepStatementSetString(2, String.valueOf(oldSkill.getUuid()));
                        dbHelper.prepStatementExecuteUpdate();
                    }
                }
                //add new skills
                for (Skill newSkill : newSkills) {
                    if (!oldSkills.contains(newSkill)) {
                        dbHelper.createPrepareStatement("INSERT INTO developer_skills (dev_id, skill_id) VALUES (?, ?);");
                        dbHelper.prepStatementSetString(1, String.valueOf(developer.getUuid()));
                        dbHelper.prepStatementSetString(2, String.valueOf(newSkill.getUuid()));
                        dbHelper.prepStatementExecuteUpdate();
                    }
                }
            }
        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public void delete(UUID id) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("DELETE FROM developers WHERE id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(id));
            dbHelper.prepStatementExecuteUpdate();

            dbHelper.createPrepareStatement("DELETE FROM developer_skills WHERE dev_id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(id));
            dbHelper.prepStatementExecuteUpdate();
        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public void deleteSkillsFromDev(UUID id) {
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("DELETE FROM developer_skills WHERE dev_id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(id));
            dbHelper.prepStatementExecuteUpdate();
        } finally {
            dbHelper.disconnect();
        }
    }

    @Override
    public Developer getById(UUID id) {
        Developer developer;
        Set<Skill> devSkills;
        String uuid;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM developers WHERE id = ?;");
            dbHelper.prepStatementSetString(1, String.valueOf(id));
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet.next()) {
                    developer = new Developer(UUID.fromString(resultSet.getString("id")), resultSet.getString("firstName"),
                            resultSet.getString("lastName"), resultSet.getString("specialty"),
                            new HashSet<Skill>(),
                            resultSet.getBigDecimal("salary"));
                    uuid = resultSet.getString("id");
                } else throw new Exception("There is no developers with that name...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }
        devSkills = selectSkillsForDev(uuid);
        developer.setSet(devSkills);
        return developer;
    }

    @Override
    public Developer getByName(String firstName, String lastName) {
        Developer developer;
        Set<Skill> devSkills;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM developers WHERE firstName = ? AND lastName = ?;");
            dbHelper.prepStatementSetString(1, firstName);
            dbHelper.prepStatementSetString(2, lastName);
            try {
                ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
                if (resultSet.next()) {
                    developer = new Developer(UUID.fromString(resultSet.getString("id")), resultSet.getString("firstName"),
                            resultSet.getString("lastName"), resultSet.getString("specialty"),
                            new HashSet<Skill>(),
                            resultSet.getBigDecimal("salary"));
                } else throw new Exception("There is no developers with that name...");
            } catch (Exception e) {
                throw new SQLRuntimeException(e);
            }
        } finally {
            dbHelper.disconnect();
        }
        devSkills = selectSkillsForDev(String.valueOf(developer.getUuid()));
        developer.setSet(devSkills);
        return developer;
    }

    @Override
    public Set<Developer> getAll() {
        Set<Developer> developers = new HashSet<>();
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT * FROM developers;");
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet != null) {
                while (resultSet.next())
                    developers.add(new Developer(UUID.fromString(resultSet.getString("id")), resultSet.getString("firstName"),
                            resultSet.getString("lastName"), resultSet.getString("specialty"),
                            new HashSet<Skill>(),
                            resultSet.getBigDecimal("salary")));
            } else throw new Exception("Developers table is empty.");
        } catch (Exception e) {
            throw new SQLRuntimeException(e);
        } finally {
            dbHelper.disconnect();
        }
        for (Developer dev : developers) {
            dev.setSet(selectSkillsForDev(String.valueOf(dev.getUuid())));
        }
        return developers;
    }

    private Set<Skill> selectSkillsForDev(String uuid) {
        Set<Skill> skills = null;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT skill_id, skills.name FROM developer_skills, skills " +
                    "WHERE dev_id = ? AND skill_id = skills.id;");
            dbHelper.prepStatementSetString(1, uuid);
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet != null) {
                skills = new HashSet<>();
                while (resultSet.next()) {
                    skills.add(new Skill(UUID.fromString(resultSet.getString("skill_id")), resultSet.getString("name")));
                }
            } else throw new Exception("There is no skills...");
            //close resultSet
            resultSet.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            dbHelper.disconnect();
        }
        return skills;
    }

    @Override
    public Skill getSkillByName(String name) {
        Skill skill;
        try {
            dbHelper.connect();
            dbHelper.createPrepareStatement("SELECT id, name FROM skills WHERE name = ?;");
            dbHelper.prepStatementSetString(1, name);
            ResultSet resultSet = dbHelper.prepStatementExecuteQuery();
            if (resultSet.next()) {
                skill = new Skill(UUID.fromString(resultSet.getString("id")), resultSet.getString("name"));
            } else throw new Exception("There is no skill with that name!");

        } catch (Exception e) {
            throw new SQLRuntimeException(e);
        } finally {
            dbHelper.disconnect();
        }
        return skill;
    }
}
