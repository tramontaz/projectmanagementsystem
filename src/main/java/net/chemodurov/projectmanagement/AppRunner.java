package net.chemodurov.projectmanagement;

import net.chemodurov.projectmanagement.dao.db.MySQLDBHelper;
import net.chemodurov.projectmanagement.view.ConsoleView;

public class AppRunner {
    public static void main(String[] args) {
        new ConsoleView(new MySQLDBHelper("jdbc", "%JDBC_tutorial0", "jdbc:mysql://localhost/jdbc?autoReconnect=true&useSSL=false"));
    }
}