package net.chemodurov.projectmanagement.controller;

import net.chemodurov.projectmanagement.dao.db.DBHelper;
import net.chemodurov.projectmanagement.service.DeveloperService;

public class DeveloperController {
    public DeveloperController(DBHelper dbHelper) {
        new DeveloperService(dbHelper);
    }
}
