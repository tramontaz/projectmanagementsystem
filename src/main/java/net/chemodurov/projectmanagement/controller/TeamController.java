package net.chemodurov.projectmanagement.controller;

import net.chemodurov.projectmanagement.dao.db.DBHelper;
import net.chemodurov.projectmanagement.service.TeamService;

public class TeamController {
    public TeamController(DBHelper dbHelper) {
        new TeamService(dbHelper);
    }
}
