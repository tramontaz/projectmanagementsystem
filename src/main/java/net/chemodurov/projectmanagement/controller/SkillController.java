package net.chemodurov.projectmanagement.controller;

import net.chemodurov.projectmanagement.dao.db.DBHelper;
import net.chemodurov.projectmanagement.service.SkillService;

public class SkillController {
    public SkillController(DBHelper dbHelper) {
        new SkillService(dbHelper);
    }
}
