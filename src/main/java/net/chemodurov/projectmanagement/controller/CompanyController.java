package net.chemodurov.projectmanagement.controller;

import net.chemodurov.projectmanagement.dao.db.DBHelper;
import net.chemodurov.projectmanagement.service.CompanyService;

public class CompanyController {
    public CompanyController(DBHelper dbHelper) {
        new CompanyService(dbHelper);
    }
}
