package net.chemodurov.projectmanagement.controller;

import net.chemodurov.projectmanagement.dao.db.DBHelper;
import net.chemodurov.projectmanagement.service.ProjectService;

public class ProjectController {
    public ProjectController(DBHelper dbHelper) {
        new ProjectService(dbHelper);
    }
}
