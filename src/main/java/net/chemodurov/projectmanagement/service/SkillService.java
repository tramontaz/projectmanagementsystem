package net.chemodurov.projectmanagement.service;

import net.chemodurov.projectmanagement.dao.SkillDAO;
import net.chemodurov.projectmanagement.dao.jdbc.JdbcSkillDAOImpl;
import net.chemodurov.projectmanagement.dao.db.DBHelper;
import net.chemodurov.projectmanagement.model.Skill;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.UUID;

public class SkillService {
    private int choice = 0;
    private String name;
    private Skill skill = null;
    private SkillDAO skillDAO;
    private BufferedReader in;

    public SkillService(DBHelper dbHelper) {
        skillDAO = new JdbcSkillDAOImpl(dbHelper);
        in = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("What do you want to do with Skill?\n" +
                "1 - Create Skill\t" + "2 - Read Skill\t" + "3 - Update Skill\t" +
                "4 - Delete Skill\t" + "5 - Show all Skills\t" + "0 - Exit");
        try {
            choice = Integer.parseInt(in.readLine());
        } catch (NumberFormatException e) {
            System.err.println("Error! Enter a positive integer with the option number!!!");
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        switch (choice) {
            case 1:
                create();
                break;
            case 2:
                read();

                break;
            case 3:
                update();
                break;
            case 4:
                delete();
                break;
            case 5:
                showAll();
                break;
            case 0:
                //exit
                System.out.println("Good bye!");
                System.exit(0);

            default:
                System.err.println("Something wrong!");
        }
    }

    private void showAll() {
        for (Skill s : skillDAO.getAll()) {
            System.out.println(s);
        }
    }

    private void delete() {
        System.out.println("Please enter the name of skill which you want to delete or '0' for exit: ");
        try {
            name = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (name.equals("0")) {
            System.out.println("Good bye!");
            System.exit(0);
        }
        try {
            skill = skillDAO.getByName(name);
            skillDAO.delete(skill.getUuid());
        } catch (Exception e) {
            System.err.println("There is no skill with that name!");
        }
    }

    private void update() {
        System.out.println("Please enter the name of skill which you want to edit: ");
        try {
            name = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String newName = null;
        try {
            skill = skillDAO.getByName(name);
        } catch (Exception e) {
            System.err.println("There is no skill with that name!");
            System.exit(0);
        }
        System.out.println("What do you want to do whit that Skill?\n" + "1 - Rename\t" + "2 - Exit");
        try {
            choice = Integer.parseInt(in.readLine());
        } catch (NumberFormatException e) {
            System.err.println("Error! Enter a positive integer with the option number!!!");
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (choice == 1) {
            System.out.println("Enter the new name of Skill " + skill.getName());
            try {
                newName = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            skill.setName(newName);
            skillDAO.update(skill);
        } else {
            System.out.println("Good bye!");
            System.exit(0);
        }
        System.out.println(skillDAO.getByName(newName));
    }

    private void read() {
        System.out.println("Please enter the name of skill which you want: ");
        try {
            name = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            skill = skillDAO.getByName(name);
            System.out.println(skill);
        } catch (Exception e) {
            System.err.println("There is no skill with that name!");
        }
    }

    private void create() {
        System.out.println("Enter the name of new skill: \t");
        try {
            name = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        skill = new Skill(UUID.randomUUID(), name);
        skillDAO.insert(skill);
    }
}
