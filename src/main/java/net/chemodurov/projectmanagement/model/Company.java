package net.chemodurov.projectmanagement.model;

import java.util.Set;
import java.util.UUID;

public class Company extends BaseEntity {
    private String name;
    private Set<Project> set;

    public Company(UUID uuid, String name, Set<Project> set) {
        super(uuid);
        this.name = name;
        this.set = set;
    }

    @Override
    public UUID getUuid() {
        return super.getUuid();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Project> getSet() {
        return set;
    }

    public void setSet(Set<Project> set) {
        this.set = set;
    }

    @Override
    public String toString() {
        return "Company:\n" +
                "\nid=" + getUuid() +
                "\nname=" + name + '\'' +
                "\nProjects:" + set +
                "\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n";
    }
}
