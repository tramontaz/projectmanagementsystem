package net.chemodurov.projectmanagement.model;

import java.util.UUID;

public class Skill extends BaseEntity{
    private String name;

    public Skill(UUID uuid, String name) {
        super(uuid);
        this.name = name;
    }

    @Override
    public UUID getUuid() {
        return super.getUuid();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getUuid() + ". " + getName();
    }
}
