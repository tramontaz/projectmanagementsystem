package net.chemodurov.projectmanagement.model;

import java.util.Set;
import java.util.UUID;

public class Project extends BaseEntity{
    private String name;
    private Set<Team> set;

    public Project(UUID uuid, String name, Set<Team> set) {
        super(uuid);
        this.name = name;
        this.set = set;
    }

    @Override
    public UUID getUuid() {
        return super.getUuid();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Team> getSet() {
        return set;
    }

    public void setSet(Set<Team> set) {
        this.set = set;
    }

    @Override
    public String toString() {
        return "Project: " +
                "\nid=" + getUuid() +
                "\nname ='" + name + '\'' +
                "\nteams:\n" + set +
                "\n========================================\n";
    }
}
