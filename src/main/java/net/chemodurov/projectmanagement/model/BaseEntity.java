package net.chemodurov.projectmanagement.model;

import java.util.UUID;

public abstract class BaseEntity {
    private UUID uuid;

    BaseEntity(UUID uuid) {
        this.uuid = uuid;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}
