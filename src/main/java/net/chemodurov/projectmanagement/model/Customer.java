package net.chemodurov.projectmanagement.model;

import java.util.Set;
import java.util.UUID;

public class Customer extends BaseEntity {
    private String firstName;
    private String lastName;
    private String address;
    private Set<Project> set;

    public Customer(UUID uuid, String firstName, String lastName, String address, Set<Project> set) {
        super(uuid);
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.set = set;
    }

    @Override
    public UUID getUuid() {
        return super.getUuid();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Project> getSet() {
        return set;
    }

    public void setSet(Set<Project> set) {
        this.set = set;
    }

    @Override
    public String toString() {
        return "Customer:\n" +
                "\nid=" + getUuid() +
                "\nfirstName='" + firstName + '\'' +
                "\nlastName='" + lastName + '\'' +
                "\naddress='" + address + '\'' +
                "\nProjects:" + set +
                "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n";
    }
}
