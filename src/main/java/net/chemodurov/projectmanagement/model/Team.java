package net.chemodurov.projectmanagement.model;

import java.util.Set;
import java.util.UUID;

public class Team extends BaseEntity{
    private String name;
    private Set<Developer> set;


    public Team(UUID uuid, String name, Set<Developer> set) {
        super(uuid);
        this.name = name;
        this.set = set;
    }

    @Override
    public UUID getUuid() {
        return super.getUuid();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Developer> getSet() {
        return set;
    }

    public void setSet(Set<Developer> set) {
        this.set = set;
    }

    @Override
    public String toString() {
        return "id=" + getUuid() +
                "\nname=" + name +
                "\nDevelopers:\n" + set;
    }
}
