USE jdbc;

CREATE TABLE skills (
  id varchar(36) NOT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id,name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE developers (
  id varchar(36) NOT NULL,
  firstName varchar(255) NOT NULL,
  lastName varchar(255) NOT NULL,
  specialty varchar(255) NOT NULL,
  salary decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE teams (
  id varchar(36) NOT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE projects (
  id varchar(36) NOT NULL,
  name varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE companies (
  id varchar(36) NOT NULL,
  name varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE customers (
  id varchar(36) NOT NULL,
  firstName varchar(225) NOT NULL,
  lastName varchar(225) NOT NULL,
  address varchar(225) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE developer_skills (
  dev_id varchar(36) NOT NULL,
  skill_id varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE team_developers (
  team_id varchar(36) NOT NULL,
  dev_id varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE project_teams (
  project_id varchar(36) NOT NULL,
  team_id varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE company_projects (
  company_id varchar(36) NOT NULL,
  project_id varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE customer_projects (
  customer_id varchar(36) NOT NULL,
  project_id varchar(36) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
